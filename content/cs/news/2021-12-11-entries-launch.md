---
title: Přihlášky on-line
---

Spustili jsme on-line [přihláškový systém](https://entries.wrc2022.rogaining.cz) závodu. Zatím se můžete pouze přihlásit, po Novém roce doplníme i možnost objednat si dopravu, ubytování doprovodu, sportovní materiál, suvenýry a další položky, které budeme schopni nabídnout.
