---
title: Nové fotky ve fotogalerii
---

Přestože převážná část závodního prostoru je ještě pokryta sněhem, už proběhlo první letošní probíhání terénu. Mrkněte na&nbsp;fotky ve&nbsp;[fotogalerii](/cs/photos/).