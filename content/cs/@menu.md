#### Menu

* [Novinky](/cs/news/)
* [Fotogalerie](/cs/photos/)
* [Informace o počasí](/cs/weather/)

::: {.logos}
[![ČAR](/images/logo-car.svg){height=75}](https://www.rogaining.cz/)

[![IRF](/images/logo-irf.svg){height=100}](https://www.rogaining.org/)
:::
