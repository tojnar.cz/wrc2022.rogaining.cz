---
title: Úvodní uvítání
---

Vítejte na webu Mistrovství světa v rogainingu 2022. Dovoluji si Vás pozvat na&nbsp;17.&nbsp;rogainingový šampionát, třetí pořádaný v&nbsp;České republice. Akce proběhne v&nbsp;malebné krajině horského hřebenu Králického Sněžníku a Rychlebských hor podél Česko-polské hranice.

COVID epidemie představuje ohromnou výzvu jak pro organizátory, tak pro&nbsp;účastníky závodu, nicméně doufám, že podmínky na&nbsp;konci léta 2022 umožní nám všem užít si závod bezpečně.

S ohledem na očekávanou eskalaci COVID pandemie během nadcházející zimy budeme přijímat přihlášky za základní startovné až do&nbsp;30.&nbsp;dubna 2022.

Přeji Vám vše nejlepší, a nashle na [Paprsku](https://www.paprsek.cz/)!

<figure><iframe width="495" height="279" src="https://www.youtube.com/embed/AlN2mlgK7vA" frameborder="0" allow="accelerometer; clipboard-write; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>

Jan Tojnar, ředitel MS 2022
