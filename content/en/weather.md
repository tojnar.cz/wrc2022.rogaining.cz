---
title: Event area weather forecast
---

##### Weather forecast
* [Weather forecast Velké Vrbno](https://www.yr.no/en/forecast/daily-table/2-3063179/Czech%20Republic/Olomouck%C3%BD/%C5%A0umperk%20District/Velk%C3%A9%20Vrbno)
* [Foreca weather Velké Vrbno](https://www.foreca.cz/Czech-Republic/Velké-Vrbno)


##### Webcams
* [Webkams at Paprsek Ski-Resort](https://www.paprsek.cz/kamery)
