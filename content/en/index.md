---
title: Welcome
---

Welcome to the WRC 2022 website. I would like to invite you to the 17<sup>th</sup> World Rogaining Championships; the 3<sup>rd</sup> World Rogaining Championships hosted by the Czech Republic. The event will take place in the marvelous landscape of the mountain ridge of Králický Sněžník and Rychleby Mountains along the Czech-Polish border.

The COVID panepidemic presents a challenge for the organizers and participants alike, nevertheless, we hope the conditions at the end of the summer 2022 will allow all of us to enjoy the event safely.
In light of the expected COVID outbreak during the upcoming northern hemisphere winter, we are going to accept basic entry fee up to April 30<sup>th</sup>, 2022.

I wish you all the best and I look forward to seeing you in the [Paprsek resort](https://www.paprsek.cz/)!

<figure><iframe width="495" height="279" src="https://www.youtube.com/embed/AlN2mlgK7vA" frameborder="0" allow="accelerometer; clipboard-write; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>

Jan Tojnar, WRC 2022 Director

