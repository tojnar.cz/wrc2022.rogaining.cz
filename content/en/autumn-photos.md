---
title: Autumn photos
---

<div class="figuregroup">
![](../images/photos/100_5384.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/100_5402.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/100_5416.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/100_5417.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/100_5421.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/100_5424.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/100_5425.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/100_5428.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3669.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3671.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3673.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3674.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3675.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3676.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3677.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3678.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3679.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3688.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3689.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3690.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3691.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3692.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3693.jpg){.thumb-sm data-lightbox="o1"}
![](../images/photos/DSCN3694.jpg){.thumb-sm data-lightbox="o1"}
</div>
