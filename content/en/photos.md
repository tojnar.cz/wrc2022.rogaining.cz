---
title: Photogallery
---

##### Photos from the event preparation

* [First survey in Spring 2022](/en/spring22-photos/)
* [Autumn map survey](/en/autumn-photos/)
* [Summer map survey](/en/summer-photos/)