---
title: Entry system on-line
---

We have launched on-line [Entry System](https://entries.wrc2022.rogaining.cz). For the time being only registration is available, after the New Year's Eve order of transportation, accompanying person accommodation and usual merchandizing will be offered (northern hemisphere compasses, sport material, souvenirs...).
