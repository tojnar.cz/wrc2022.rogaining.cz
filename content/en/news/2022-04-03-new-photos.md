---
title: New photos
---

Although snow covers the largest part of the event area, the first this year terrain check was started. Enjoy the photos in&nbsp;the&nbsp;[event&nbsp;photogallery](/en/photos/).