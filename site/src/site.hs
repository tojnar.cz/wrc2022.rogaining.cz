{-# LANGUAGE OverloadedStrings #-}

import Common
import Control.Applicative ((<|>))
import Data.Functor ((<&>))
import Data.List.Extra (dropPrefix, dropSuffix)
import Data.Maybe (fromMaybe)
import Data.Time.Format (TimeLocale (..), defaultTimeLocale)
import qualified GHC.IO.Encoding as E
import Hakyll
import Hakyll.Contrib.Tojnar.ExternalMetadata (externalMetadataField)
import Hakyll.Contrib.Tojnar.Gallery (figureGroups, implicitFiguresWith, bs5GalleryStyle)
import Hakyll.Contrib.Tojnar.Menu (menuField)
import Hakyll.Contrib.Tojnar.Thumbnail (makeThumbnails)
import HierarchicalMenu (matchHierarchicalMenu, hierarchicalMenuField)
import LanguageChooser (languageChooserField)
import System.FilePath ((</>), (-<.>), splitExtension, takeFileName)
import Text.Pandoc.Extensions (Extension (Ext_implicit_figures), disableExtension)
import Text.Pandoc.Options (readerExtensions)

config :: Configuration
config =
	defaultConfiguration {
		destinationDirectory = "public",
		previewHost = "0.0.0.0"
	}

rootUri :: String
rootUri = "https://wrc2022.rogaining.cz/"

languages :: [String]
languages =
	[
	"cs",
	"en"
	]

main :: IO ()
main = do
	E.setLocaleEncoding E.utf8

	hakyllWith config $ do
		match ("content/files/*" .||. "content/images/*") $ do
			route stripContentDirectory
			compile copyFileCompiler

		match menuPattern $ do
			compile markdownCompiler

		matchHierarchicalMenu "content/*/topmenu.yaml"

		match contentPagesPattern $ version menuVersion $ compile destination

		-- Match pages that are not posts.
		match (contentPagesPattern .&&. complement postPattern) $ do
			route $ stripContentDirectory `composeRoutes` turnIntoSubdirectory `composeRoutes` setExtension "html"
			compile $ markdownCompiler
				>>= loadAndApplyTemplate "templates/layout.html" pageContext

		-- Match news posts.
		match postPattern $ do
			route $ stripContentDirectory `composeRoutes` turnIntoSubdirectory `composeRoutes` rmDateRoute `composeRoutes` setExtension "html"
			compile $ do
				fp <- getResourceFilePath
				let lang = getContentLocale (dropPrefix "./" fp)

				markdownCompiler
					>>= saveSnapshot contentSnapshot
					>>= loadAndApplyTemplate "templates/post.html" (postContext lang)
					>>= loadAndApplyTemplate "templates/layout.html" pageContext

		version menuVersion $ create (map (fromFilePath . (\lang -> "content" </> lang </> "news/index.html")) languages) $ do
			compile destination

		-- Create news page for each supported language.
		create (map (fromFilePath . (\lang -> "content" </> lang </> "news/index.html")) languages) $ do
			route stripContentDirectory
			compile $ do
				fp <- getResourceFilePath
				let lang = getContentLocale (dropPrefix "./" fp)

				let metadataFile = fromFilePath ("content" </> lang </> "metadata.yaml")
				newsTitle <- fromMaybe "" <$> getMetadataField metadataFile "newsTitle"

				posts <- recentFirst =<< loadAllSnapshots (postPattern .&&. hasNoVersion .&&. languagePattern lang) contentSnapshot

				let indexContext =
					listField "posts" (postContext lang) (return posts)
					<> bodyField "body"
					<> metadataField
					<> missingField
				let context =
					constField "title" newsTitle
					<> pageContext

				makeItem ""
					>>= loadAndApplyTemplate "templates/posts.html" indexContext
					>>= loadAndApplyTemplate "templates/layout.html" context

		-- Create ATOM feed for each supported language.
		create (map (fromFilePath . (\lang -> "content" </> lang </> "feed.atom")) languages) $ do
			route stripContentDirectory
			compile $ do
				fp <- getResourceFilePath
				let lang = getContentLocale (dropPrefix "./" fp)

				let metadataFile = fromFilePath ("content" </> lang </> "metadata.yaml")
				title <- fromMaybe "" <$> getMetadataField metadataFile "feedTitle"
				description <- fromMaybe "" <$> getMetadataField metadataFile "feedDescription"

				let feedCtx =
					postUrlField
					<> postContext lang
					<> bodyField "description"

				posts <- fmap (take 10) . recentFirst =<< loadAllSnapshots (postPattern .&&. hasNoVersion .&&. languagePattern lang) contentSnapshot
				renderAtom (feedConfiguration title description) feedCtx posts

		match "templates/*" $ compile templateCompiler

		version redirectsVersion $ createRedirects brokenLinks

		version redirectsVersion $ createNetlifyRedirects brokenLinks

contentSnapshot :: Snapshot
contentSnapshot = "content"

-- Take out the yyyy-mm-dd part from the post URL
rmDateRoute :: Routes
rmDateRoute = gsubRoute "/[0-9]{4}-[0-9]{2}-[0-9]{2}-" (const "/")

feedConfiguration :: String -> String -> FeedConfiguration
feedConfiguration title description =
	FeedConfiguration {
		feedTitle = title,
		feedDescription = description,
		feedAuthorName = "",
		feedAuthorEmail = "",
		feedRoot = rootUri
	}

-- | List of redirects to generate.
brokenLinks :: [(Identifier, String)]
brokenLinks =
	[
	-- Root redirects
	("index.html", "/en/")
	]

postPattern :: Pattern
postPattern = "content/*/news/*.md"

languagePattern :: String -> Pattern
languagePattern lang = fromGlob ("content" </> lang </> "**")

pageContext :: Context String
pageContext =
	externalMetadataField
	<> activeClassField
	<> languageChooserField languages
	<> localeField
	<> hierarchicalMenuField "topMenu"
	<> menuField
	<> defaultContext

-- | Defines a context for posts
postContext :: String -> Context String
postContext lang = do
	postSlugField
	<> localizedDateField "date" lang
	<> dateField "isodate" "%Y-%m-%d"
	<> defaultContext

postSlugField :: Context a
postSlugField = field "slug" $ \item -> do
	url <- getRoute (itemIdentifier item)
	return (takeFileName (dropSuffix "/index.html" (fromMaybe "" url)))

postUrlField :: Context a
postUrlField = field "url" $ \item -> do
	url <- getRoute (itemIdentifier item)
	return (dropSuffix "index.html" (fromMaybe "foo" url))

-- | Get URL for current Item
destination :: Compiler (Item String)
destination =
	setVersion Nothing <$> getUnderlying
	>>= identifierDestination
	<&> fromMaybe routeNotSet
	>>= makeItem

identifierDestination :: Identifier -> Compiler (Maybe String)
identifierDestination identifier =
	getRoute identifier
	<&> fmap (('/':) . dropSuffix "index.html")

-- | Defines a 'activeClass' field, which returns 'active'
-- when the path passed as argument matches the current page.
-- Otherwise returns an empty string.
activeClassField :: Context String
activeClassField =
	functionField "activeClass" $ \[p] _ -> do
		path <- toFilePath <$> getUnderlying
		return $ if path == p then "active" else ""

-- | Defines a 'locale' field that holds the code of the locale of the current page.
localeField :: Context String
localeField =
	field "locale" $ \_ -> do
		path <- toFilePath <$> getUnderlying
		return $ getContentLocale path

-- | Replace path to a HTML file by a path to 'index.html' file in a directory named after the file.
turnIntoSubdirectory :: Routes
turnIntoSubdirectory =
	customRoute $ \identifier ->
		let
			path = toFilePath identifier
			(dirname, ext) = splitExtension path
		in
			if takeFileName dirname == "index" then
				path
			else
				dirname </> "index" -<.> ext


-- | (Not yet) customizec markdown compiler.
markdownCompiler :: Compiler (Item String)
markdownCompiler = pandocCompilerWithTransformM readOpts writeOpts filters
    where
        readOpts = defaultHakyllReaderOptions { readerExtensions = disableExtension Ext_implicit_figures (readerExtensions defaultHakyllReaderOptions) }
        writeOpts = defaultHakyllWriterOptions
        filters = makeThumbnails . implicitFiguresWith bs5GalleryStyle . figureGroups

localizedDateField :: String -> String -> Context a
localizedDateField fieldName lang =
	dateFieldWith locale fieldName (dateFmt locale)
	where
		locale = timeLocale lang

timeLocale :: String -> TimeLocale
timeLocale "cs" =
	defaultTimeLocale {
		months = [
			("ledna", "led"),
			("února", "úno"),
			("března", "bře"),
			("dubna", "dub"),
			("května", "kvě"),
			("června", "čer"),
			("července", "čec"),
			("srpna", "srp"),
			("září", "zář"),
			("října", "říj"),
			("listopadu", "lis"),
			("prosince", "pro")
		],
		dateFmt = "%e. %B %Y"
	}
timeLocale "en" =
	defaultTimeLocale {
		dateFmt = "%B %e, %Y"
	}
timeLocale _ =
	defaultTimeLocale

createNetlifyRedirects :: [(Identifier, String)] -> Rules ()
createNetlifyRedirects redirects = do
	create ["_redirects"] $ do
		route idRoute
		compile $ do
			entries <- mapM makeEntry redirects
			makeItem (unlines entries)

	where
		makeEntry (ident, target) = do
			dest <- identifierDestination ident
			redirDest <- identifierDestination (setVersion (Just redirectsVersion) ident)
			return (fromMaybe routeNotSet (dest <|> redirDest) <> " " <> target)
