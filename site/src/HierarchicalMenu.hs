{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module HierarchicalMenu (
	hierarchicalMenuField,
	matchHierarchicalMenu
) where

import Common (getContentLocale)
import Data.Aeson (Options (fieldLabelModifier), defaultOptions, genericParseJSON, genericToEncoding, genericToJSON)
import Data.Binary (Binary)
import Data.Char (toLower)
import Data.List.Extra (dropPrefix, mconcatMap)
import Data.Maybe (isJust)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Data.Typeable (Typeable)
import Data.Yaml (FromJSON (parseJSON), ToJSON (toJSON, toEncoding), decodeEither', encode, prettyPrintParseException)
import GHC.Generics (Generic)
import Hakyll (Compiler, Context, Item (Item), Pattern, Rules, Writable, compile, field, fromFilePath, getResourceBody, getUnderlying, loadBody, match, toFilePath, write, withItemBody)
import Hakyll.Core.Compiler.Internal (compilerThrow)
import System.FilePath ((</>))
import Text.Blaze.Html ((!), (!?), Html, toHtml)
import qualified Text.Blaze.Html5 as Html
import qualified Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Pretty (renderHtml)

data MenuItem = MenuItem {
	menuItemLabel :: String,
	menuItemUrl :: Maybe String,
	menuItemChildren :: Maybe [MenuItem]
} deriving (Binary, Eq, Generic, Show, Typeable)

customOptions :: Options
customOptions =
	defaultOptions
		{
			fieldLabelModifier = map toLower . dropPrefix "menuItem"
		}

instance FromJSON MenuItem where
	parseJSON = genericParseJSON customOptions

instance ToJSON MenuItem where
	toJSON = genericToJSON customOptions
	toEncoding = genericToEncoding customOptions

instance Writable [MenuItem] where
	write path (Item identifier item) = do
		write path (Item identifier (encode item))

menuCompiler :: Compiler (Item [MenuItem])
menuCompiler = do
	body <- getResourceBody
	withItemBody (either (compilerThrow . pure . prettyPrintParseException) return . decodeEither' . encodeUtf8 . T.pack) body

matchHierarchicalMenu :: Pattern -> Rules ()
matchHierarchicalMenu pattern = do
	match pattern $ do
		compile menuCompiler

-- | A field rendering a HTML of hierarchical menu.
hierarchicalMenuField :: String -> Context a
hierarchicalMenuField fieldName =
	field fieldName (\_item -> do
		identifier <- getUnderlying
		let path = toFilePath identifier

		body <- loadBody (fromFilePath ("content" </> getContentLocale path </> "topmenu.yaml"))

		return (renderHtml (mconcatMap (menuItemToHtml 0) (body :: [MenuItem])))
	)

menuItemToHtml :: Int -> MenuItem -> Html
menuItemToHtml level (MenuItem label murl mchildren) =
	Html.li
		! A.class_ (Html.stringValue (unwords liClassList))
		$ do
			Html.a
				! A.class_ (Html.stringValue (unwords linkClassList))
				!? (hasChildren, A.role "button")
				!? (hasChildren, Html.customAttribute "data-bs-toggle" "dropdown")
				!? (hasChildren, Html.customAttribute "aria-haspopup" "true")
				!? (hasChildren, Html.customAttribute "aria-expanded" "false")
				! A.href (maybe "#" Html.stringValue murl)
				$ toHtml label
			submenu level mchildren
	where
		hasChildren = isJust mchildren

		liClassList =
			(if level == 0 then ["nav-item"] else [])
			<> (if hasChildren then [if level == 0 then "dropdown" else "dropend"] else [])
		linkClassList =
			[if level == 0 then "nav-link" else "dropdown-item"]
			<> (if hasChildren then ["dropdown-toggle"] else [])

		submenu lvl (Just children) =
			Html.ul
				! A.class_ "dropdown-menu"
				$ mconcatMap (menuItemToHtml (lvl + 1)) children
		submenu _ Nothing = mempty
