{
  description = "Website of WRC 2022";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    hakyll-contrib-tojnar.url = "gitlab:tojnar.cz/hakyll-contrib-tojnar";

    netlify-deployer-src = {
      url = "gitlab:lepovirta/netlify-deployer";
      flake = false;
    };

    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , flake-compat
    , hakyll-contrib-tojnar
    , netlify-deployer-src
    , nixpkgs
    , utils
    }:

    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        haskellPackages = pkgs.haskellPackages.override {
          overrides = pkgs.lib.composeExtensions (final: prev: {
            hakyll = pkgs.lib.pipe prev.hakyll [
              (p: pkgs.haskell.lib.addBuildDepend p prev.graphviz)
              (p: pkgs.haskell.lib.overrideSrc p rec {
                version = "4.14.1.0";
                src = pkgs.fetchFromGitHub {
                  owner = "jaspervdj";
                  repo = "hakyll";
                  rev = "v${version}";
                  sha256 = "Zz1TWbKFF/Im9hCCNoLwdjvNNz9NSl3P+Vv4KLzP7Z8=";
                };
              })
            ];

            wrc2022-rogaining-cz = final.callPackage ./wrc2022-rogaining-cz.nix { };
          }) hakyll-contrib-tojnar.haskellOverlay;
        };

        netlify-deployer = pkgs.buildGoModule rec {
          pname = "netlify-deployer";
          version = builtins.toString netlify-deployer-src.lastModified;

          src = netlify-deployer-src;

          nativeBuildInputs = [
            pkgs.makeWrapper
          ];

          vendorSha256 = "cy6PEnr5KpDWaRWI2BzGpnqKhTE3xzaCufbFXEq58AY=";

          postInstall = ''
            cp gitlab-deploy-site.sh "$out/bin/gitlab-deploy-site"
            wrapProgram "$out/bin/gitlab-deploy-site" \
              --prefix PATH : "$out/bin" \
              --set-default NETLIFY_MAIN_BRANCH main \
              --set-default NETLIFY_DIRECTORY public
          '';
        };
      in {
        devShells = {
          default = pkgs.mkShell {
            buildInputs = [
              self.packages.${system}.default

              # For installing frontend depenendencies
              pkgs.nodejs
              pkgs.python3 # for node-gyp

              # For deploy
              netlify-deployer
            ];
          };

          haskell = self.packages.${system}.wrc2022-rogaining-cz.env.overrideAttrs (attrs: {
            nativeBuildInputs = attrs.nativeBuildInputs ++ [
              pkgs.cabal-install
            ];
          });
        };

        packages = {
          inherit netlify-deployer;

          wrc2022-rogaining-cz = haskellPackages.wrc2022-rogaining-cz;

          default = self.packages.${system}.wrc2022-rogaining-cz;
        };

        apps = {
          default = utils.lib.mkApp {
            drv = self.packages.${system}.wrc2022-rogaining-cz;
          };
        };
      }
  );

  nixConfig = {
    extra-substituters = "https://tojnar-cz.cachix.org";
    extra-trusted-public-keys = "tojnar-cz.cachix.org-1:639adw4WNe39ElsKtaLuTQpEWwKp9MVZ/snx9nMidZs=";
  };
}
