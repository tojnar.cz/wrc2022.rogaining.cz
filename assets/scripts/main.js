import lightGallery from 'lightgallery';
import { Dropdown } from 'bootstrap';

document.addEventListener("DOMContentLoaded", () => {
	const links = document.querySelectorAll('[data-lightbox]');
	let galleries = new Map();

	links.forEach((link) => {
		const group = link.getAttribute('data-lightbox');
		let groupList;
		if (!galleries.has(group)) {
			groupList = [];
			galleries.set(group, groupList);
		} else {
			groupList = galleries.get(group);
		}
		const img = link.querySelector('img');
		groupList.push({
			src: link.getAttribute('href'),
			subHtml: img.getAttribute('alt'),
			downloadUrl: img.getAttribute('data-original'),
			linkEl: link,
		});
	})

	galleries.forEach((gallery) => {
		const dynamicGallery = lightGallery(document.body, {
			dynamic: true,
			dynamicEl: gallery,
			loop: false,
		});

		gallery.forEach((item, index) => {
			console.log(item);
			item.linkEl.addEventListener('click', (event) => {
				event.preventDefault();
				dynamicGallery.openGallery(index);
			});
		})
	});

	document.querySelectorAll('.dropdown-toggle').forEach((dropdownToggleEl) => {
		new Dropdown(dropdownToggleEl);
	});
});
